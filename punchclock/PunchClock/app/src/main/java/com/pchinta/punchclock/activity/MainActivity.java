package com.anurag.punchclock.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.anurag.punchclock.PunchClockApplication;
import com.anurag.punchclock.R;
import com.anurag.punchclock.service.UserService;
import com.anurag.punchclock.view.PCProgressBar;
import com.radiusnetworks.proximity.ProximityKitBeacon;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;


public class MainActivity extends Activity {
    public static final String TAG = "MainActivity";
    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static boolean isRunning = false;
    public static String username = null;
    public static String deviceId = null;
    /**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     */
    String SENDER_ID = "397981596585";

    GoogleCloudMessaging gcm;
    AtomicInteger msgId = new AtomicInteger();
    SharedPreferences prefs;
    Context context;

    String regid;


//    Map<String, TableRow> rowMap = new HashMap<String, TableRow>();

    public static String getUsername() {
        return username;
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        deviceId = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        setContentView(R.layout.activity_main);
        context = getApplicationContext();
        prefs = getGCMPreferences(context);
        ((PunchClockApplication) getApplication()).setMainActivity(this);

        final EditText fullnameView = (EditText) findViewById(R.id.text_fullName);


        username = prefs.getString(getString(R.string.username), null);
        if (username == null || username.isEmpty()) {

            Log.i(TAG, "Device Id is " + deviceId);
            final PCProgressBar progressBar = new PCProgressBar(MainActivity.this);
            new AsyncTask<Void, Void, Void>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    progressBar.show();

                }

                @Override
                protected Void doInBackground(Void... params) {
                    username = UserService.getInstance().getUserName(deviceId);
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    if (username != null) {
//                        saveUsername();
                        if (username != null && !username.isEmpty()) {
//                            Intent intent = new Intent(MainActivity.this, PunchTimeActivity.class);
//                            startActivity(intent);
                            fullnameView.setText(username);
                        }
                    }
                    progressBar.hide();
                    super.onPostExecute(aVoid);
                }
            }.execute();
        } else {
            fullnameView.setText(username);
        }


        Button login = (Button) findViewById(R.id.button_login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fullnameView.getText() != null && fullnameView.getText().length() != 0) {
                    username = fullnameView.getText().toString().trim();
                    saveUsername();
                    if (!isRunning) {
                        startManager();
                        isRunning = true;
                    }
                    Intent intent = new Intent(MainActivity.this, PunchTimeActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, "Please enter a name", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void saveUsername() {

        // Check device for Play Services APK. If check succeeds, proceed with
        //  GCM registration.
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(getString(R.string.username), username);
        editor.commit();
        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(context);

            if (regid.isEmpty()) {
                registerInBackground();
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }
//
//
//        final MainActivity that = this;
//        final PCProgressBar progressBar = new PCProgressBar(MainActivity.this);
//        new AsyncTask<Void, Void, Void>() {
//
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//                progressBar.show();
//            }
//
//            @Override
//            protected Void doInBackground(Void... params) {
//                UserService.getInstance().save(username, deviceId);
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(Void aVoid) {
////                final SharedPreferences sharedPref = that.getPreferences(Context.MODE_PRIVATE);
////                SharedPreferences.Editor editor = sharedPref.edit();
////                editor.putString(getString(R.string.username), username);
////                editor.commit();
//                progressBar.hide();
//                super.onPostExecute(aVoid);
//            }
//        }.execute();
    }

    public void displayTableRow(final ProximityKitBeacon beacon, final String displayString, final boolean updateIfExists) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                TableLayout table = (TableLayout) findViewById(R.id.beacon_table);
                // You could instead call beacon.toString() which includes the identifiers
                String key = beacon.getId1().toString() + "-" +
                        beacon.getId2().toInt() + "-" + beacon.getId3().toInt();
//                TableRow tr = (TableRow) rowMap.get(key);
//                if (tr == null) {
//                    tr = new TableRow(MainActivity.this);
//                    tr.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
//                    rowMap.put(key, tr);
//                    table.addView(tr);
//                } else {
//                    if (updateIfExists == false) {
//                        return;
//                    }
//                }
//                tr.removeAllViews();
//                TextView textView = new TextView(MainActivity.this);
//                textView.setText(displayString);
//                tr.addView(textView);
            }
        });
    }

    /**
     * Button action which turn the Proximity Kit manager service on and off.
     *
     * @param view button object which was pressed
     */
    public void toggleManager(View view) {
//        if (view.getId() != R.id.manager_toggle) { return; }

        if (isRunning) {
            stopManager();
            isRunning = false;
        } else {
            startManager();
            isRunning = true;
        }
    }

    /**
     * Turn the Proximity Kit manager on and update the UI accordingly.
     */
    private void startManager() {
        PunchClockApplication app = (PunchClockApplication) getApplication();
//        Button btn = (Button) findViewById(R.id.manager_toggle);

        app.startManager();
//        btn.setText(R.string.manager_toggle_stop);
    }

    /**
     * Turn the Proximity Kit manager off and update the UI accordingly.
     */
    private void stopManager() {
        PunchClockApplication app = (PunchClockApplication) getApplication();
//        TableLayout table = (TableLayout) findViewById(R.id.beacon_table);
//        Button btn = (Button) findViewById(R.id.manager_toggle);

        app.stopManager();
//        table.removeAllViews();
//        rowMap.clear();
//        btn.setText(R.string.manager_toggle_start);
    }

    /**
     * Gets the current registration ID for application on GCM service.
     * <p/>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     * registration ID.
     */
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing registration ID is not guaranteed to work with
        // the new app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the registration ID in your app is up to you.
        return getSharedPreferences(MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p/>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            final PCProgressBar progressBar = new PCProgressBar(MainActivity.this);

            @Override
            protected void onPreExecute() {
                progressBar.show();
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;

                    // You should send the registration ID to your server over HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your app.
                    // The request to your server should be authenticated if your app
                    // is using accounts.
                    String error = sendRegistrationIdToBackend();

                    // For this demo: we don't need to send it because the device
                    // will send upstream messages to a server that echo back the
                    // message using the 'from' address in the message.

                    // Persist the registration ID - no need to register again.
                    if (error == null) {
                        storeRegistrationId(context, regid);
                    } else {
                        msg = error;
                    }
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
//                mDisplay.append(msg + "\n");
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
                progressBar.hide();
            }
        }.execute(null, null, null);
    }

    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
     * or CCS to send messages to your app. Not needed for this demo since the
     * device sends upstream messages to a server that echoes back the message
     * using the 'from' address in the message.
     */
    private String sendRegistrationIdToBackend() {
        // Your implementation here.
        String error = UserService.getInstance().save(username, deviceId, regid);
        return error;
    }

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId   registration ID
     */
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        if(isRunning) {
            stopManager();
            isRunning = false;
        }
        super.onDestroy();
    }
}
