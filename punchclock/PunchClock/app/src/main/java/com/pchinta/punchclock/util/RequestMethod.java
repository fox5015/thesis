package com.anurag.punchclock.util;

/**
 * Created by anurag on 3/3/15.
 */
public enum RequestMethod {
    GET, POST
}
