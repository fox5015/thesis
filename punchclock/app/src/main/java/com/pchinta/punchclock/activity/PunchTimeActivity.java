package com.anurag.punchclock.activity;

import android.app.ListActivity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.anurag.punchclock.R;
import com.anurag.punchclock.adapter.UserStatusAdapter;
import com.quickblox.sample.chat.ui.activities.NewDialogActivity;
import com.quickblox.sample.chat.ui.activities.SplashActivity;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class PunchTimeActivity extends ListActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    // This is the Adapter being used to display the list's data
    UserStatusAdapter mAdapter;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private TextView locationView;
    private TextView titleView;
    private ImageButton chatButton;

    // These are the Contacts rows that we will retrieve

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        setContentView(R.layout.activity_punch_time);
        mGoogleApiClient.connect();
        locationView = (TextView) findViewById(R.id.text_location);
        titleView = (TextView) findViewById(R.id.text_noOfPeople);
        chatButton = (ImageButton) findViewById(R.id.button_chat);
        chatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PunchTimeActivity.this, SplashActivity.class);
                startActivity(intent);
            }
        });
        ImageButton refresh = (ImageButton) findViewById(R.id.button_refresh);
        mAdapter = new UserStatusAdapter(this);
        setListAdapter(mAdapter);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapter.load();
            }
        });

        mAdapter.load();
    }

    @Override
    protected void onResume() {
        mAdapter.load();
        super.onResume();
    }

    public TextView getTitleView() {
        return titleView;
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            double longitude = mLastLocation.getLongitude();
            double latitude = mLastLocation.getLatitude();
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            try {
                List<Address> listAddresses = geocoder.getFromLocation(latitude, longitude, 1);
                if (null != listAddresses && listAddresses.size() > 0) {
                    Address address = listAddresses.get(0);
                    String _Location = address.getAddressLine(0)+ ", " + address.getAddressLine(1) +", " + address.getAddressLine(2);
                    locationView.setText(_Location);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onBackPressed() {
        // Do Here what ever you want do on back press;
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
