package com.anurag.punchclock.adapter;

import android.content.Context;
import android.database.DataSetObserver;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.anurag.punchclock.R;
import com.anurag.punchclock.activity.MainActivity;
import com.anurag.punchclock.activity.PunchTimeActivity;
import com.anurag.punchclock.model.UserStatus;
import com.anurag.punchclock.service.UserService;
import com.anurag.punchclock.view.PCProgressBar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by anurag on 3/1/15.
 */
public class UserStatusAdapter extends BaseAdapter implements ListAdapter {


    private List<UserStatus> usersWithStatus = new ArrayList<>();
    private PunchTimeActivity mContext;

    private LayoutInflater mLayoutInflater;


    public UserStatusAdapter(PunchTimeActivity context) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        return usersWithStatus.size();
    }

    @Override
    public Object getItem(int position) {
        return usersWithStatus.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout itemView = null;
        if (convertView == null) {
            itemView = (LinearLayout) mLayoutInflater.inflate(R.layout.list_item, parent, false);
        } else {
            itemView = (LinearLayout) convertView;
        }
        final UserStatus us = usersWithStatus.get(position);
        TextView nameView = (TextView) itemView.findViewById(R.id.text_name);
        ImageView eyeView = (ImageView) itemView.findViewById(R.id.image_eye);
        final ImageButton bellView = (ImageButton) itemView.findViewById(R.id.image_bell);
        final UserStatusAdapter that = this;
        bellView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                that.followUser(us.getName(), !us.isWatchedByYou());
                us.setWatchedByYou(!us.isWatchedByYou());
                if (us.isWatchedByYou())
                    bellView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_bell_on));
                else
                    bellView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_bell_off));
            }
        });
        TextView switchView = (TextView) itemView.findViewById(R.id.switch_in);
        nameView.setText(us.getName());
        if (us.isWatchingYou())
            eyeView.setVisibility(View.VISIBLE);
        else
            eyeView.setVisibility(View.INVISIBLE);
        if (us.isWatchedByYou())
            bellView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_bell_on));
        else
            bellView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_bell_off));

        switchView.setText(us.isIn() ? "IN":"OUT");
        return itemView;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    public void followUser(final String followUser, final boolean follow) {
        final UserStatusAdapter that = this;
        new AsyncTask<Void, Void, Void>() {
            PCProgressBar progressBar = new PCProgressBar(mContext);

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.show();
            }

            @Override
            protected Void doInBackground(Void... params) {
                UserService service = UserService.getInstance();
                if (follow)
                    service.followUser(MainActivity.getUsername(), followUser);
                else
                    service.unFollowUser(MainActivity.getUsername(), followUser);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                progressBar.hide();
                super.onPostExecute(aVoid);
            }
        }.execute();

    }


    public void load() {
        final UserStatusAdapter that = this;
        new AsyncTask<Void, Void, Void>() {
            PCProgressBar progressBar = new PCProgressBar(mContext);

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.show();
            }

            @Override
            protected Void doInBackground(Void... params) {
                UserService service = UserService.getInstance();
                try {
                    usersWithStatus.clear();
                    usersWithStatus.addAll(service.getUsersWithStatus(MainActivity.getUsername()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                progressBar.hide();
                that.notifyDataSetChanged();
                that.mContext.getListView().invalidateViews();
                mContext.getTitleView().setText(usersWithStatus.size() + " People");
                super.onPostExecute(aVoid);
            }
        }.execute();

    }
}
