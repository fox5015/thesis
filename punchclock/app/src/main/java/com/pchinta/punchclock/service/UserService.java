package com.anurag.punchclock.service;

import android.util.Log;

import com.anurag.punchclock.model.UserStatus;
import com.anurag.punchclock.util.RequestMethod;
import com.anurag.punchclock.util.RestClient;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by anurag on 3/1/15.
 */
public class UserService {

    private static final String TAG = "UserService";
    private static UserService instance = null;

    private UserService() {

    }

    public static UserService getInstance() {
        if (instance == null) {
            instance = new UserService();
        }
        return instance;
    }

    public List<UserStatus> getUsersWithStatus(String username) throws IOException {

        RestClient client = new RestClient("/user/findAllUsersWithStatus");
        client.addParam("username", username);

        try {
            client.Execute(RequestMethod.GET);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String response = client.getResponse();
        if (client.getResponseCode() == 200) {
            ObjectMapper objectMapper = new ObjectMapper();
            List<UserStatus> userStatuses = objectMapper.readValue(response, new TypeReference<List<UserStatus>>() {
            });
            return userStatuses;
        } else {
            Log.e(TAG, client.getResponseCode() + "");
            Log.e(TAG, client.getResponse());
            return new ArrayList<>();
        }

    }

    public String followUser(String username, String followUser) {
        RestClient client = new RestClient("/user/follow");
        client.addParam("currentuser", username);
        client.addParam("tofollowuser", followUser);
        try {
            client.Execute(RequestMethod.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (client.getResponseCode() != 200) {
            Log.e(TAG, client.getResponseCode() + "");
            return client.getResponse();
        } else {
            return null;
        }
    }

    public String unFollowUser(String username, String followUser) {
        RestClient client = new RestClient("/user/unfollow");
        client.addParam("currentuser", username);
        client.addParam("tofollowuser", followUser);
        try {
            client.Execute(RequestMethod.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (client.getResponseCode() != 200) {
            Log.e(TAG, client.getResponseCode() + "");
            return client.getResponse();
        } else {
            return null;
        }
    }

    public String getUserName(String deviceId) {
        RestClient client = new RestClient("/user/findByDevice");
        client.addParam("deviceId", deviceId);
        try {
            client.Execute(RequestMethod.GET);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String response = client.getResponse();
        if (client.getResponseCode() == 200) {
            return response.trim().replaceAll("\n", "");
        } else {
            Log.e(TAG, client.getResponseCode() + "");
            return null;
        }
    }

    public String save(String username, String deviceId, String regId) {
        RestClient client = new RestClient("/user/save");
        client.addParam("username", username);
        client.addParam("deviceId", deviceId);
        client.addParam("regId", regId);
        try {
            client.Execute(RequestMethod.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (client.getResponseCode() != 200) {
            Log.e(TAG, client.getResponseCode() + "");
            return client.getResponse();
        } else {
            return null;
        }
    }

    public String sendUpdateNearBeacon(String username, boolean isNear) {
        RestClient client = new RestClient("/user/updateNearBeacon");
        client.addParam("username", username);
        client.addParam("isNear", String.valueOf(isNear));
        try {
            client.Execute(RequestMethod.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (client.getResponseCode() != 200) {
            Log.e(TAG, client.getResponseCode() + "");
            return client.getResponse();
        } else {
            return null;
        }
    }

    public String sendUpdateInsideRegion(String username, boolean isInside) {
        RestClient client = new RestClient("/user/updateInsideGeofence");
        client.addParam("username", username);
        client.addParam("isInside", String.valueOf(isInside));
        try {
            client.Execute(RequestMethod.POST);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (client.getResponseCode() != 200) {
            Log.e(TAG, client.getResponseCode() + "");
            return client.getResponse();
        } else {
            return null;
        }
    }
}
