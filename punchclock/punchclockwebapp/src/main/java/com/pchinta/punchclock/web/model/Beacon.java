package com.anurag.punchclock.web.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "PC_BEACON", uniqueConstraints = @javax.persistence.UniqueConstraint(name = "PC_BEACON_UK", columnNames = { "NAME" }))
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@NamedQueries(value = {
		@NamedQuery(name = "findAllBeacons", query = "SELECT beacon FROM Beacon beacon"),
		@NamedQuery(name = "findAllBeaconsByName", query = "SELECT beacon FROM Beacon beacon where beacon.name=:name") })
public class Beacon {

	@Version
	@Column(name = "VERSION")
	private Integer version;

	@Column(name = "NAME", nullable = false, length = 50)
	private String name;

	@Id
	@Column(name = "DEVICEID", nullable = false, length = 255)
	private String deviceId;

	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST,
			CascadeType.REFRESH })
	@JoinTable(name = "PC_USER_BEACON", joinColumns = { @JoinColumn(name = "BEACON_ID", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "USER_NAME", nullable = false, updatable = false) })
	private List<User> nearByUsers;

	public String getName() {
		return name;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object obj) {
		return this.name.equals(((Beacon) obj).name);
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return this.name.hashCode();
	}

}
