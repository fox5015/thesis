package com.anurag.punchclock.web.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "PC_USER", uniqueConstraints = @javax.persistence.UniqueConstraint(name = "PC_USER_UK", columnNames = { "USERNAME" }))
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@NamedQueries(value = {
		@NamedQuery(name = "findAllUsers", query = "SELECT user FROM User user"),
		@NamedQuery(name = "findUserByDeviceId", query = "SELECT user FROM User user where user.deviceId=:deviceId"),
		@NamedQuery(name = "findUserByName", query = "SELECT user FROM User user where user.name=:username"),
		@NamedQuery(name = "findAllFollowedByUser", query = "SELECT user FROM User user INNER JOIN FETCH user.following following where user.name=:name"),
		@NamedQuery(name = "findAllFollowingThisUser", query = "SELECT user FROM User user INNER JOIN FETCH user.followedBy followedBy where user.name=:name") })
public class User {

	@Version
	@Column(name = "VERSION")
	private Integer version;

	@Column(name = "USERNAME", nullable = false, length = 50)
	private String name;

	@Id
	@Column(name = "DEVICEID", nullable = false, length = 255)
	private String deviceId;

	@Column(name = "CHATID", nullable = false, length = 255)
	private String chatId;

	public String getChatId() {
		return chatId;
	}

	public void setChatId(String chatId) {
		this.chatId = chatId;
	}

	@Column(name = "REG_ID", nullable = false, length = 255)
	private String regId;

	@Column(name = "STATUS")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean status;

	@Column(name = "ISNEAR")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean near;

	@Column(name = "ISINSIDE")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean inside;

	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST,
			CascadeType.REFRESH })
	@JoinTable(name = "PC_USER_BEACON", joinColumns = { @JoinColumn(name = "USER_NAME", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "BEACON_ID", nullable = false, updatable = false) })
	private List<Beacon> nearByBeacons;

	@JsonIgnore
	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST,
			CascadeType.REFRESH })
	@JoinTable(name = "PC_USER_FOLLOW", joinColumns = { @JoinColumn(name = "USER_NAME", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "FOLLOWING", nullable = false, updatable = false) })
	private List<User> following;

	@JsonIgnore
	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST,
			CascadeType.REFRESH })
	@JoinTable(name = "PC_USER_FOLLOW", joinColumns = { @JoinColumn(name = "FOLLOWING", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "USER_NAME", nullable = false, updatable = false) })
	private List<User> followedBy;

	public String getName() {
		return name;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Boolean getStatus() {
		if (status == null)
			return new Boolean(true);
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Boolean isNear() {
		if (near == null)
			return Boolean.FALSE;
		return near;
	}

	public void setNear(Boolean isNear) {
		this.near = isNear;
	}

	public List<User> getFollowing() {
		return following;
	}

	public void setFollowing(List<User> following) {
		this.following = following;
	}

	public List<User> getFollowedBy() {
		return followedBy;
	}

	public void setFollowedBy(List<User> followedBy) {
		this.followedBy = followedBy;
	}

	public String getRegId() {
		return regId;
	}

	public void setRegId(String regId) {
		this.regId = regId;
	}

	public Boolean isInside() {
		if (inside == null)
			return Boolean.FALSE;
		return inside;
	}

	public void setInside(Boolean inside) {
		this.inside = inside;
	}

	@Override
	public boolean equals(Object obj) {
		return this.name.equals(((User) obj).name);
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return this.name.hashCode();
	}

}
