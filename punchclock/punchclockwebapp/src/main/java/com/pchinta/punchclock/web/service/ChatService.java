package com.anurag.punchclock.web.service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONObject;

public class ChatService {

	public static final long APPLICAION_ID = 20372;
	public static final String AUTH_KEY = "e6pdtztOJuYZYzQ";
	public static final String AUTH_SECRET = "QaRPUY4SXt6akZU";

	public static String hmacDigest(String msg, String keyString, String algo) {
		String digest = null;
		try {
			SecretKeySpec key = new SecretKeySpec(
					(keyString).getBytes("UTF-8"), algo);
			Mac mac = Mac.getInstance(algo);
			mac.init(key);

			byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));

			StringBuffer hash = new StringBuffer();
			for (int i = 0; i < bytes.length; i++) {
				String hex = Integer.toHexString(0xFF & bytes[i]);
				if (hex.length() == 1) {
					hash.append('0');
				}
				hash.append(hex);
			}
			digest = hash.toString();
		} catch (UnsupportedEncodingException e) {
		} catch (InvalidKeyException e) {
		} catch (NoSuchAlgorithmException e) {
		}
		return digest;
	}

	public static String getSignature() {
		long nonce = Math.round(33432 * Math.random());
		return hmacDigest(
				"application_id=" + APPLICAION_ID + "&auth_key=" + AUTH_KEY
						+ "&nonce=" + (int) nonce + "&timestamp="
						+ System.currentTimeMillis(), AUTH_SECRET, "sha1");
	}

	public static String authenticate() throws UnsupportedEncodingException {
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost("https://api.quickblox.com/session.json");
		JSONObject json = new JSONObject();
		json.put("application_id", APPLICAION_ID);
		json.put("auth_key", AUTH_KEY);
		post.setEntity(new StringEntity(json.toJSONString()));
		return null;

	}
}
