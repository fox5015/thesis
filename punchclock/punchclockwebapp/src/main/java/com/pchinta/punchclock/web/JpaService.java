package com.anurag.punchclock.web;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JpaService {

	private static Logger logger = LoggerFactory.getLogger(JpaService.class);

	private static JpaService instance = null;

	public static JpaService getJPAService() {
		if (instance == null) {
			instance = new JpaService();
		}

		return instance;
	}

	public <T> T persist(T model) throws JPAException {
		T model1 = null;
		EntityManager entityManager = null;
		EntityManagerFactory entityManagerFactory = EMFService
				.getEntityManagerFactory();
		try {
			entityManager = entityManagerFactory.createEntityManager();
			entityManager.getTransaction().begin();
			model1 = entityManager.merge(model);
			entityManager.persist(model1);
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			if (e instanceof SQLException) {
				SQLException nextException = ((SQLException) e)
						.getNextException();
				logger.error(nextException.getMessage(), nextException);
			}
			if (null != entityManager && entityManager.isOpen()) {
				if (entityManager.getTransaction().isActive())
					entityManager.getTransaction().rollback();
			}
			throw new JPAException(e);
		} finally {
			if (null != entityManager && entityManager.isOpen()) {
				entityManager.close();
			}
		}
		return model1;
	}

	public <T> T delete(T model) throws JPAException {
		T model1 = null;
		EntityManager entityManager = null;
		EntityManagerFactory entityManagerFactory = EMFService
				.getEntityManagerFactory();
		try {
			entityManager = entityManagerFactory.createEntityManager();
			entityManager.getTransaction().begin();
			model1 = entityManager.merge(model);
			entityManager.remove(model1);
			entityManager.getTransaction().commit();
			entityManager.close();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			if (null != entityManager && entityManager.isOpen()) {
				if (entityManager.getTransaction().isActive())
					entityManager.getTransaction().rollback();
			}
			throw new JPAException(e);
		} finally {
			if (null != entityManager && entityManager.isOpen()) {
				entityManager.close();
			}
		}
		return model1;
	}

	public <T> T findById(Class<T> modelClass, String id) throws JPAException {
		T model1 = null;
		EntityManager entityManager = null;
		EntityManagerFactory entityManagerFactory = EMFService
				.getEntityManagerFactory();
		try {
			entityManager = entityManagerFactory.createEntityManager();
			model1 = entityManager.find(modelClass, id);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			if (null != entityManager && entityManager.isOpen()) {
				if (entityManager.getTransaction().isActive())
					entityManager.getTransaction().rollback();
			}
			throw new JPAException(e);
		} finally {
			if (null != entityManager && entityManager.isOpen()) {
				entityManager.close();
			}
		}
		return model1;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> findAll(Class<T> modelClass, String namedQuery,
			Map<String, Object> parameters) throws JPAException {
		List<T> models = null;

		EntityManager entityManager = null;
		try {
			EntityManagerFactory entityManagerFactory = EMFService
					.getEntityManagerFactory();
			entityManager = entityManagerFactory.createEntityManager();
			boolean hasParameters = false;

			if (null != parameters && !parameters.isEmpty())
				hasParameters = true;

			Query query = entityManager.createNamedQuery(namedQuery);

			if (hasParameters) {
				for (String name : parameters.keySet()) {
					query.setParameter(name, parameters.get(name));
				}
			}

			models = query.getResultList();
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.getMessage(), e);
			throw new JPAException(e);
		} finally {
			if (null != entityManager && entityManager.isOpen())
				entityManager.close();
		}
		return models;
	}

	// public long findCount(String namedQuery, Map<String, Object> parameters)
	// throws JPAException {
	// long count = 0;
	// EntityManager entityManager = null;
	// try {
	// EntityManagerFactory entityManagerFactory = EMFService
	// .getEntityManagerFactory();
	// entityManager = entityManagerFactory.createEntityManager();
	// boolean hasParameters = false;
	//
	// if (null != parameters && !parameters.isEmpty())
	// hasParameters = true;
	//
	// Query query = entityManager.createNamedQuery(namedQuery);
	//
	// if (hasParameters) {
	// for (String name : parameters.keySet()) {
	// query.setParameter(name, parameters.get(name));
	// }
	// }
	//
	// count = (long) query.getSingleResult();
	// } catch (Exception e) {
	// // e.printStackTrace();
	// logger.error(e.getMessage(), e);
	// throw new JPAException(e);
	// } finally {
	// if (null != entityManager && entityManager.isOpen())
	// entityManager.close();
	// }
	// return count;
	// }

	public <T> List<T> findAll(Class<T> class1, String namedQuery,
			Map<String, Object> params, int page) throws JPAException {
		List<T> models = null;

		EntityManager entityManager = null;
		try {
			EntityManagerFactory entityManagerFactory = EMFService
					.getEntityManagerFactory();
			entityManager = entityManagerFactory.createEntityManager();
			boolean hasParameters = false;

			if (null != params && !params.isEmpty())
				hasParameters = true;

			Query query = entityManager.createNamedQuery(namedQuery);

			if (hasParameters) {
				for (String name : params.keySet()) {
					query.setParameter(name, params.get(name));
				}
			}
			query.setMaxResults(15);
			query.setFirstResult(15 * page);
			models = query.getResultList();
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error(e.getMessage(), e);
			throw new JPAException(e);
		} finally {
			if (null != entityManager && entityManager.isOpen())
				entityManager.close();
		}
		return models;
	}
}
