package com.anurag.punchclock.web;


public class JPAException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JPAException(Exception e) {
		super(e.getMessage(), e);
	}

	public JPAException(String message, Exception e) {
		super(message, e);
	}

	public JPAException(String message) {
		super(message);
	}
}
