package com.anurag.punchclock.web.model;

public class UserStatus {
	private String name;
	private boolean isIn;
	private boolean isWatchingYou;
	private boolean isWatchedByYou;

	public boolean isIn() {
		return isIn;
	}

	public void setIn(boolean isIn) {
		this.isIn = isIn;
	}

	public boolean isWatchingYou() {
		return isWatchingYou;
	}

	public void setWatchingYou(boolean isWatchingYou) {
		this.isWatchingYou = isWatchingYou;
	}

	public boolean isWatchedByYou() {
		return isWatchedByYou;
	}

	public void setWatchedByYou(boolean isWatchedByYou) {
		this.isWatchedByYou = isWatchedByYou;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
