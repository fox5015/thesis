package com.anurag.punchclock.web;

import java.net.URL;
import java.util.List;
import java.util.Properties;

import javax.persistence.SharedCacheMode;
import javax.persistence.ValidationMode;
import javax.persistence.spi.ClassTransformer;
import javax.persistence.spi.PersistenceUnitInfo;
import javax.persistence.spi.PersistenceUnitTransactionType;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PCPersistenceUnitInfo implements PersistenceUnitInfo {
	Logger logger = LoggerFactory.getLogger(PCPersistenceUnitInfo.class);

	public void addTransformer(ClassTransformer arg0) {
		// TODO Auto-generated method stub

	}

	public boolean excludeUnlistedClasses() {
		// TODO Auto-generated method stub
		return true;
	}

	public ClassLoader getClassLoader() {
		ClassLoader cl = null;
		try {
			cl = Thread.currentThread().getContextClassLoader();
			// cl = HibernatePersistenceProvider.class.getClassLoader();
		} catch (Throwable ex) {
			// Cannot access thread context ClassLoader - falling back to system
			// class loader...
		}
		if (cl == null) {
			// No thread context class loader -> use class loader of this class.
			cl = this.getClass().getClassLoader();
		}
		return cl;
	}

	public List<URL> getJarFileUrls() {
		// TODO Auto-generated method stub
		return null;
	}

	public DataSource getJtaDataSource() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<String> getManagedClassNames() {
		// TODO Auto-generated method stub
		try {
			return SchemaGenerator.getEntityClasses();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public List<String> getMappingFileNames() {
		// TODO Auto-generated method stub
		return null;
	}

	public ClassLoader getNewTempClassLoader() {
		// TODO Auto-generated method stub
		return null;
	}

	public DataSource getNonJtaDataSource() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getPersistenceProviderClassName() {
		return "org.hibernate.ejb.HibernatePersistence";
	}

	public String getPersistenceUnitName() {
		// TODO Auto-generated method stub
		return "SimpleLogin";
	}

	public URL getPersistenceUnitRootUrl() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getPersistenceXMLSchemaVersion() {
		// TODO Auto-generated method stub
		return null;
	}

	public Properties getProperties() {

		Properties properties = new Properties();
		properties.put("hibernate.bytecode.use_reflection_optimizer", "false");

		properties.put("hibernate.jdbc.batch_size", "2000");
		properties.put("hibernate.order_inserts", "true");
		properties.put("hibernate.order_updates", "true");

		properties.put("hibernate.connection.release_mode", "auto");
		properties.put("hibernate.check_nullability", "false");

		// int max_size = Integer.parseInt(ConfigurationUtil
		// .getProperty("connection.pool.size"));
		int max_size = 5;
		if (max_size > 0) {
			properties.put("hibernate.c3p0.max_size", "" + max_size);
			properties.put("hibernate.c3p0.min_size", "1");
			properties.put("hibernate.c3p0.timeout", "5000");
			properties.put("hibernate.c3p0.max_statements", "100");
			properties.put("hibernate.c3p0.idle_test_period", "300");
			properties.put("hibernate.c3p0.acquire_increment", "2");
			properties.put("hibernate.c3p0.acquireRetryAttempts", "3");
			properties.put("hibernate.c3p0.acquireRetryDelay", "250");
		}

		String dbName = System.getProperty("RDS_DB_NAME");
		String userName = System.getProperty("RDS_USERNAME");
		String password = System.getProperty("RDS_PASSWORD");
		String hostname = System.getProperty("RDS_HOSTNAME");
		String port = System.getProperty("RDS_PORT");
		try {
			String jdbcdriver = ConfigurationUtil.getProperty("jdbc.driver");
			String jdbcUrl = "jdbc:postgresql://" + hostname + ":" + port + "/"
					+ dbName;

			properties.put("hibernate.connection.driver_class", jdbcdriver);
			// properties.put("hibernate.hbm2ddl.auto", "create");
			properties.put("hibernate.connection.url", jdbcUrl);

			properties.put("hibernate.connection.username", userName);
			properties.put("hibernate.connection.password", password);
			// properties.put("hibernate.cache.region.factory_class",
			// "net.sf.ehcache.hibernate.SingletonEhCacheRegionFactory");
			// properties.put("hibernate.cache.use_structured_entries",
			// "false");
			// properties.put("hibernate.cache.use_second_level_cache",
			// "false");
			// properties.put("hibernate.cache.use_query_cache", "true");
			// properties.put("shared-cache-mode", "ALL");
			// properties.put("hibernate.generate_statistics", "true");
			// properties.put("net.sf.ehcache.configurationResourceName",
			// "/jpaCache.xml");
			properties.put("hibernate.cache.region_prefix", "PC_JPA");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return properties;
	}

	public SharedCacheMode getSharedCacheMode() {
		// TODO Auto-generated method stub
		return null;
	}

	public PersistenceUnitTransactionType getTransactionType() {
		return PersistenceUnitTransactionType.RESOURCE_LOCAL;
	}

	public ValidationMode getValidationMode() {
		// TODO Auto-generated method stub
		return null;
	}

}
