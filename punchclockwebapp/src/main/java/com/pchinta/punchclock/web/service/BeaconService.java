package com.anurag.punchclock.web.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.anurag.punchclock.web.JPAException;
import com.anurag.punchclock.web.JpaService;
import com.anurag.punchclock.web.model.Beacon;

@Path("/Beacon")
public class BeaconService {

	private JpaService jpaService = JpaService.getJPAService();
	private static Logger logger = LoggerFactory.getLogger(BeaconService.class);

	ObjectMapper objectMapper = new ObjectMapper();

	@Path("/save")
	@POST
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.TEXT_PLAIN)
	public boolean save(@QueryParam("name") String beaconName,
			@QueryParam("deviceId") String deviceId) throws JPAException,
			JsonGenerationException, JsonMappingException, IOException {
		// validate Beacon
		Beacon beacon = findByName(beaconName);
		if (beacon != null) {
			if (!deviceId.equals(beacon.getDeviceId())) {
				beacon.setDeviceId(deviceId);
				jpaService.persist(beacon);
			}
			return true;
		} else {
			beacon = new Beacon();
			beacon.setName(beaconName);
			beacon.setDeviceId(deviceId);
			jpaService.persist(beacon);
			return true;
		}
	}

	@Path("/delete/{beaconname}")
	@POST
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.TEXT_PLAIN)
	public boolean delete(@PathParam("beaconname") String beaconName)
			throws JPAException, JsonGenerationException, JsonMappingException,
			IOException {
		// validate Beacon
		Beacon beacon = findByName(beaconName);
		if (beacon != null) {
			jpaService.delete(beacon);
			return true;
		}
		return true;
	}

	@Path("/fetchAll")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String fetchAll() throws JPAException, JsonGenerationException,
			JsonMappingException, IOException {
		List<Beacon> allBeacons = jpaService.findAll(Beacon.class,
				"findAllBeacons", null);

		return objectMapper.writeValueAsString(allBeacons);
	}

	@Path("/findByName")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Beacon findByName(String name) throws JPAException,
			JsonGenerationException, JsonMappingException, IOException {
		logger.debug("Fetching Beacon with email : " + name);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("name", name);
		List<Beacon> Beacons = jpaService.findAll(Beacon.class,
				"findAllBeaconsByName", parameters);
		Beacon Beacon = null;
		if (Beacons != null && !Beacons.isEmpty()) {
			Beacon = Beacons.get(0);
		}
		return Beacon;
	}
}
