package com.anurag.punchclock.web;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("service")
public class PunchClockRestApp extends ResourceConfig {

	public PunchClockRestApp() {
		packages("com.anurag.punchclock.web.service");
	}

}
