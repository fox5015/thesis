package com.anurag.punchclock.web;

import javax.persistence.EntityManagerFactory;

import org.hibernate.jpa.HibernatePersistenceProvider;

public class EMFService {

	private static EntityManagerFactory emf;

	public static EntityManagerFactory getEntityManagerFactory() {
		if (emf == null) {
			PCPersistenceUnitInfo pInfo = new PCPersistenceUnitInfo();
			emf = new HibernatePersistenceProvider()
					.createContainerEntityManagerFactory(pInfo,
							pInfo.getProperties());
		}

		return emf;
	}

}
