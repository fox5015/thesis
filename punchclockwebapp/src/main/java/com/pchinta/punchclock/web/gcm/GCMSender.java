package com.anurag.punchclock.web.gcm;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;

public class GCMSender {

	private static final String API_KEY = "AIzaSyAvJhnKCQWjkbjbQDb0FuDIEmfyxtuSNfA";

	private static final String GCM_URL = "https://android.googleapis.com/gcm/send";

	public static void sendNotification(List<String> targets, String data)
			throws ClientProtocolException, IOException {
		try {

			Sender sender = new Sender(API_KEY);

			// use this line to send message with payload data
			Message message = new Message.Builder()
			// .collapseKey("message")
			// .timeToLive(241000)
					.delayWhileIdle(true).addData("text", data).build();

			// Use this for multicast messages
			MulticastResult result = sender.send(message, targets, 1);

			System.out.println(result.toString());
			if (result.getResults() != null) {
				int canonicalRegId = result.getCanonicalIds();
				if (canonicalRegId != 0) {
				}
			} else {
				int error = result.getFailure();
				System.out.println(error);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
