package com.anurag.punchclock.web;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigurationUtil {

	private static Properties properties = null;

	public static String getProperty(String name) throws IOException {
		if (properties == null) {
			properties = new Properties();

			InputStream is = ConfigurationUtil.class.getClassLoader()
						.getResourceAsStream("configuration.properties");
			properties.load(is);
			is.close();

		}
		Object value = properties.get(name);
		if(value == null)
			value = System.getProperty(name);
		return (String) value;
	}

}
